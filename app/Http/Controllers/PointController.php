<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreValidateXMLDOC;
use App\Service\Point\ServiceGetPointAir;
use Exception;

class PointController extends Controller
{
    public function index()
    {
       return view('index');
    }

    /**
     * @var ServiceGetPointAir
     */
    private $serviceGetPointAir;

    public function __construct(ServiceGetPointAir $serviceGetPointAir)
    {
        $this->serviceGetPointAir = $serviceGetPointAir;
    }

    /**
     * @param StoreValidateXMLDOC $validateXMLDOC
     * @return array
     * @throws Exception
     */
    public function point(StoreValidateXMLDOC $validateXMLDOC): array
    {
        try {
            return $this->serviceGetPointAir->GetEndPoint($validateXMLDOC->file('import_file')->getContent());
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
