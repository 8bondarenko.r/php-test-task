<?php

namespace App\Service\XML;

interface InterfaceParseData
{
    public function parsData(array $data);
}
