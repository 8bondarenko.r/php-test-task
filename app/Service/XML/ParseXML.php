<?php

namespace App\Service\XML;

use Exception;

class ParseXML implements InterfaceParseData
{

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function parsData(array $data): array
    {
        $filteredData = [];
        try {
            foreach ($data['AirSegments']['AirSegment'] as $segment) {
                $filteredData[] = [
                    'timestamp' => strtotime($segment['Arrival']['@attributes']['Date'] . ' ' . $segment['Arrival']['@attributes']['Time']),
                    'Board' =>  (string)$segment['Board']['@attributes']['City'],
                    'Off' =>(string)$segment['Off']['@attributes']['City'] ,
                ];
            }

        }catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $filteredData;
    }
}
