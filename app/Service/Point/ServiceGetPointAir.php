<?php

namespace App\Service\Point;

use App\Service\XML\ParseXML;
use Exception;
use Mtownsend\XmlToArray\XmlToArray;

class ServiceGetPointAir implements InterfaceGetPoint
{
    public $startPoint;
    public $endPoint;

    /**
     * @var ParseXML
     */
    public $parser;

    public function __construct(ParseXML $parser)
    {
        $this->parser = $parser;
    }

    /**
     * @throws Exception
     */
    public function GetEndPoint($data): array
    {
        return [
            'end_point' => $this->endPoint($this->parser->parsData(XmlToArray::convert($data))),
            'return_way' => $this->returnWay($this->parser->parsData(XmlToArray::convert($data)))
        ];
    }

    /**
     * @param $data
     * @return null[]
     * @throws Exception
     */
    public function endPoint($data): array
    {
        $break_point_city = null;
        $timePeriod = 0;
        $way = [];

        foreach ($data as $datum) {
            $this->startPoint = $datum['Board'];
            $next = next($data);
            if ($next) {
                $timeArrival = $next['timestamp'] - $datum['timestamp'];
                if ($timeArrival > $timePeriod) {
                    $timePeriod = $timeArrival;
                    $this->endPoint = $way['end_point_city'] = $datum['Off'];
                }
                if ($next['Board'] != $datum['Off'] && is_null($break_point_city)) {
                    $break_point_city = $way['break_point_city'] = $datum['Off'];
                }
            }
        }

        return $way;
    }

    public function returnWay($data): string
    {
        $routBack = [];

        foreach ($data as $index => $datum) {
            $next = next($data);
            if($this->endPoint === $next['Board']) {
                $newDataRoute = array_slice($data, $index+1);

                foreach ($newDataRoute as $rotes) {
                    $routBack[] = $rotes['Board'];
                    $routBack[] = $rotes['Off'];

                    if($this->startPoint === $rotes['Off']) {
                        continue;
                    }
                }
            }
        }

        return implode(' - ', $routBack);
    }
}
